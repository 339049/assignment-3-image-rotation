#include "../include/fileoc.h"
#include <errno.h>
#include <string.h>

int file_open(FILE ** file,const char *filename, const char *mode){
    if (!file || !filename || !mode) {
        return 0;
    }
        
    *file = fopen(filename, mode);
    if (!*file) {
        fprintf(stderr, "The file %s does not open\n%s\n", filename, strerror(errno));
        return 0;
    }
    return 1;
}


int file_close(FILE *file){       
    if (fclose(file) == 0) 
        return 0;
    fprintf(stderr, "The file does not close\n%s\n"  ,strerror(errno));
    return 1;   // Код ошибки и так возвращается, enum status code рекомендуется, но не обязательно (написано в README)
}
