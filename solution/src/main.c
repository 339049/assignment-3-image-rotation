#include <stdio.h>

#include "../include/bmp.h"
#include "../include/fileoc.h"
#include "../include/image.h"
#include "../include/rotate.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Arguments count error");
        return 0;
    }

    FILE *input, *output;
    int const open_input_file_code = file_open(&input, argv[1], "rb");
    int const open_output_file_code = file_open(&output, argv[2], "wb");

    if (!open_input_file_code) {
        if (open_output_file_code)
            file_close(output);
        return 0;
    }

    if (!open_output_file_code) {
        file_close(input);
        return 0;
    }

    struct image img = {0};
    enum read_status rstat = from_bmp(input, &img);
    if (rstat != 0) {
        fprintf(stderr, "Invalid bmp file %s", argv[1]);
        image_free(img);
        file_close(input);
        file_close(output);
        return 0;
    }
    struct image new_img = rotate(img);

    if (new_img.data != NULL) {
        enum write_status wstat = to_bmp(output, &new_img);
        if (wstat != 0) {
            fprintf(stderr, "Something went wrong while writing to file %s", argv[2]);
            image_free(img);
            image_free(new_img);
            file_close(input);
            file_close(output);
            return 0;
        }
    }

    image_free(img);
    image_free(new_img);

    file_close(input);
    file_close(output);
    return 0;
}
