#include "../include/bmp.h"

#include <stdlib.h>

static uint8_t calculate_padding(uint64_t width) {
    return (4 - width * sizeof(struct pixel) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};

    if (fread(&header, sizeof(header), 1, in) != 1)
        return READ_INVALID_HEADER;
    if (header.bfType != BMP_SIGNATURE)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount != BMP_24_BITS_COLORS)
        return READ_INVALID_BITS;
    if (header.biSize != BMP_DIB_HEADER_SIZE)
        return READ_INVALID_HEADER;

    *img = image_create(header.biWidth, header.biHeight);
    if (img->data != NULL) {
        const uint8_t PADDING = calculate_padding(header.biWidth);

        if (fseek(in, header.bOffBits, SEEK_SET))
            return READ_INVALID_HEADER;

        for (uint64_t i = 0; i < img->height; i++) {
            if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width)
                return READ_INVALID_HEADER;
            if (fseek(in, PADDING, SEEK_CUR))
                return READ_INVALID_HEADER;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    const uint8_t PADDING = calculate_padding(img->width);
    struct bmp_header header = {
        .bfType = BMP_SIGNATURE,
        .bfileSize = sizeof(header) + img->width * img->height * sizeof(struct pixel) + PADDING * img->height,
        .bOffBits = sizeof(header),
        .biSize = BMP_DIB_HEADER_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biBitCount = BMP_24_BITS_COLORS,
        .biSizeImage = img->width * img->height * sizeof(struct pixel) + PADDING * img->height,
        .biXPelsPerMeter = BMP_PIXELS_PER_METER_W,
        .biYPelsPerMeter = BMP_PIXELS_PER_METER_H};
    if (fwrite(&header, sizeof(header), 1, out) != 1)
        return WRITE_ERROR;

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;

        // if (fseek(out, PADDING, SEEK_CUR))
        //     return WRITE_ERROR;

        // Вместо смещения можно записывать мусор в качестве PADDING'а
        // Тогда в конце файла смещение также точно будет
        if (fwrite(img->data, 1, PADDING, out) != PADDING)
            return WRITE_ERROR;
    }
    
    return WRITE_OK;
}
