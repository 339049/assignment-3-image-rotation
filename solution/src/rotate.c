#include "../include/rotate.h"
#include <stdlib.h>

struct image rotate(struct image const source) {
    struct image new_img = image_create(source.height, source.width);
    if (new_img.data != NULL) {
        for (uint64_t h_ind = 0; h_ind < new_img.height; h_ind++) {
            for (uint64_t w_ind = 0; w_ind < new_img.width; w_ind++) {
                new_img.data[h_ind * new_img.width + w_ind] = source.data[new_img.height * (new_img.width - w_ind - 1) + h_ind];
            }
        }    
    }
    return new_img;
}
