#include "../include/image.h"

#include<stdlib.h>

struct image image_create(uint64_t width, uint64_t height)
{
    struct image img = {.width = width, .height = height};
    img.data = malloc(img.width*img.height*sizeof(struct pixel));
    return img;
}

void image_free(struct image image)
{
    if(image.data != NULL)
        free(image.data);
}

