#pragma once
#include <stdio.h>

/*
    0 - failed opening 
    1 - successful opening
*/
int file_open(FILE ** file, const char *filename, const char *mode);

/*
    0 - failed closing 
    1 - successful closing
*/
int file_close(FILE *file);
