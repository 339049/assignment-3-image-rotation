#pragma once
#include <stdint.h>

struct __attribute__((packed)) pixel
{
    uint8_t b, g, r;
};

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

struct image image_create(uint64_t width, uint64_t height);


void image_free(struct image image);

